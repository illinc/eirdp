# Чтобы скрипт не начинался с ключевого слова function
ans = 42;

# добиваем v нулями с хвоста до длины x
function [vlong] = vexpand(v, x)
	vlong = 0*x;
	vlong(1:length(v)) = v;
endfunction;



# «Подсвечивание» графика с номером line_idx в массива графиков h и утончение всех других
function sel(line_idx, h)	
	for hline=h
		set(hline, "linewidth", 1);
	endfor;
	set(h(line_idx), "linewidth", 3);
endfunction;

# Выборка по 1-му октету octet из Z (какие именно пути — с коррекцией или без — определяется в функции histip)
# (x — принимаемые значения, v — частоты)
function [v] = oneoctet(octet, x, Z)
try
	i = (Z(:,1)==octet); 
	z = Z(i,:);
	[vnn] = histip(x, z(:,2:3));
	v = reshape(vnn, 1, length(x));
catch
	octet
	z
end_try_catch;	
endfunction;

# Выборка по набору 1-х октетов octets из Z (x — принимаемые значения, v — частоты)
function [v] = multioctet(octets, x, Z)
	v = 0*x;
	for oc=octets
		v = v + oneoctet(oc, x, Z);
	endfor;
endfunction;

# Тест выборки v
# x — принимаемые значения, v — частоты
# k = [kmin kmax] — min и max кол-во разбиений
# verbose — 0 — без подробного вывода тестов хи-квадрат, 1 — с оным
function [n, m, s, summary] = full_test(v, x, k, verbose)
	n = sum(v);
	[m, s] = pdf_params(x, v);
	try
	summary = [0 0 0];	# [good bad total] на случай сбоя
	[summary] = ChiSquare_alltest(v, k(1):k(2), verbose);	
	catch
	end_try_catch;
endfunction;

function [y_new, ynn_new, ylegend_new, idx] = addnormdata(v_norm, vlegendtitle, y, ynn, ylegend)
	
	y_new = [y; v_norm];
	ynn_new = [ynn; v_norm*0];
	ylegend_new = [ylegend; vlegendtitle];
	
	idx = size(y_new,1);
endfunction;
# Добавление выборки v (x — принимаемые значения, v — частоты) с параметрами
# 	n — кол-во узлов, m — параметр сдвига, s — масштабный, 
# 	summary — статистика по тестам хи-квадрат [за H0/против/всего]
# 	и заголовком vtitle
# в массивы:
# 	v/n	→ y — нормированное распределение;
#  	v	→ ynn — ненормированное распределение;
#  	vtitle + n, m, s, summary	→ ylegend — подпись для легенды;
# (суффикс _new для избежания конфликта).
function [y_new, ynn_new, ylegend_new, idx] = adddata(v, vtitle, n, m, s, summary, x, y, ynn, ylegend)
	vlegend = sprintf("%s [%3d/%3d/%3d] (n=%3d,m=%0.1f,s=%0.1f)", vtitle, summary(1), summary(2), summary(3), n, m, s);
	
	y_new = [y; v/n];
	ynn_new = [ynn; v];
	ylegend_new = [ylegend; vlegend];
	
	idx = size(y_new,1);
endfunction;

# Тест выборки v и добавление выборки в массивы y, ynn, ylegend
# (full_test + adddata)
function [y_new, ynn_new, ylegend_new, idx, n, m, s, summary] = test_and_adddata(v, vtitle, x, y, ynn, ylegend, k, verbose)
	[n, m, s, summary] = full_test(v, x, k, verbose);
	[y_new, ynn_new, ylegend_new, idx] = adddata(v, vtitle, n, m, s, summary, x, y, ynn, ylegend);
endfunction;

# Тест выборки, состоящей из нескольких групп по 1-м октетам IP (их список — octets);
# добавление выборки в массивы y, ynn, ylegend
# и вывод на экран полученных сведений
# (формирование выборки + full_test + adddata + printf)
function [y_new, ynn_new, ylegend_new, idx, summary] = multioctet_add_test(octets, x, y, ynn, ylegend, Z, k, verbose)
	v = multioctet(octets, x, Z);
	vtitle = strtrim(sprintf("%d ", octets));
	[y_new, ynn_new, ylegend_new, idx, n, m, s, summary] = test_and_adddata(v, vtitle, x, y, ynn, ylegend, k, verbose);
		
# 	printf("%24s: n=%3d, μ=%0.1f, σ=%0.1f k=%2d-%2d %3d/%3d/%3d\n", 
# 		vtitle, n, m, s, k(1), k(2), summary(1), summary(2), summary(3));
endfunction;

function print_data(y, ynn, ylegend)
	printf(" № Выборка   [%3s/%3s/%3s] (n  μ  σ)\n", "H0", "H1", "всего");
	wid = size(ylegend, 2);
	for i=1:size(y,1)
		printf("%2d %*s\n", i, wid, strtrim(ylegend(i,:)));
	endfor;
endfunction;

function show_ip_groups(octetsample, is_full_output, threshold_n_y)
	printf("Октет        n  μ  σ  k   %3s/%3s/%3s\n", "H0", "H1", "всего")
	for i=1:size(octetsample,1)
		sample=octetsample(i,:);
		if (is_full_output | sample(2) > threshold_n_y)	# n > threshold_n_y
			printf("ip=%3d.x.x.x, n=%3d, μ=%0.1f, σ=%0.1f  k=%2d-%2d  %3d/%3d/%3d\n", sample);
		end;
	endfor;
endfunction;


function vline(x, style)
	if (nargin < 2) style="-k"; endif;
	plot ([x; x], [0; 1], style);
endfunction;

function [h] = vstem(x, v, style)
	if (nargin < 3) style="-k"; endif;
	h = stem(x, v(x), style);
endfunction;

# https://ccrma.stanford.edu/~jos/sasp/Matlab_Parabolic_Peak_Interpolation.html
function [p,y,a] = qint(ym1,y0,yp1) 
%QINT - quadratic interpolation of three adjacent samples
%
% [p,y,a] = qint(ym1,y0,yp1) 
%
% returns the extremum location p, height y, and half-curvature a
% of a parabolic fit through three points. 
% Parabola is given by y(x) = a*(x-p)^2+b, 
% where y(-1)=ym1, y(0)=y0, y(1)=yp1. 

p = (yp1 - ym1)/(2*(2*y0 - yp1 - ym1)); 
y = y0 - 0.25*(ym1-yp1)*p;
a = 0.5*(ym1 - 2*y0 + yp1);
endfunction;

# http://www.eng.cam.ac.uk/help/tpl/programs/Matlab/minmax.html
# Find local min and max values of experimental data with Matlab

function minima = findminima(x)
%FINDMINIMA  Find location of local minima
%  From David Sampson
%  See also FINDMAXIMA

minima = findmaxima(-x);
endfunction;

function maxima = findmaxima(x)
%FINDMAXIMA  Find location of local maxima
%  From David Sampson
%  See also FINDMINIMA

% Unwrap to vector
x = x(:);
% Identify whether signal is rising or falling
upordown = sign(diff(x));
% Find points where signal is rising before, falling after
maxflags = [upordown(1)<0; diff(upordown)<0; upordown(end)>0];
maxima   = find(maxflags);

endfunction;






function [p]=fit_and_show(v, x)
	n = sum(v);
	mv = -1;
	[ml, sl] = logn_params(x, v);	# логнормальное распределение
	[pl] = logn_discrete_pdf(ml, sl);	# логнормальное распределение

	[mn, sn] = normal_params(x, v);	# нормальное распределение
	[pn] = normal_discrete_pdf(mn, sn);	# нормальное распределение
	
# 	printf("Параметры логнормальной аппроксимации: μ=%0.1f, σ=%0.1f; нормальной μ=%0.1f, σ=%0.1f\n", ml, sl, mn, sn);
	
	y = [v/n; pl(x); pn(x)];
	ylegend = [sprintf("v (n=%d)", n); sprintf("logn (m=%0.1f,s=%0.1f)", ml, sl); sprintf("normal (m=%0.1f,s=%0.1f)", mn, sn)];
	
	[p] = v/n; # за неимением лучшего
	


# 	p = Peak_LogNormal4Parameter_model(x);
# 	y = [y; p(x)];
# 	ylegend = [ylegend; "Log-Normal 4 Parameter 2D"];
# 	
# 	printarray("v-4logn", y(1,:)-p);
# 	y = [y; y(1,:)-p];
# 	ylegend = [ylegend; "v-4logn"];

# 	figure(11);
	
	h = plot(x, y);
	axis([0,max(x), 0, max(max(y))]);
	legend(ylegend);
	set(h(1), "linewidth", 4);
	
# 	hold on;
# 	[mdn imdn] = discrete_mode(pn);
# 	[mdl imdl] = discrete_mode(pl);
# 	[mdv imdv] = discrete_mode(v/n);
# 	
# # 	printf("             LN    N     V\n");
# # 	printf("Мода (пик)  %5.2f %5.2f %5.2f\n", imdl, imdn, imdv);
# # 	printf("Модосреднее %5.2f %5.2f %5.2f\n", mdl, mdn, mdv);
# # 	printf("Теор.мода   %5.2f %5.2f %5.2f\n", exp(ml - sl^2), mn, mv);
# 	printf("Модосреднее LN=%5.2f N=%5.2f V=%5.2f\n", mdl, mdn, mdv);
# 	vline([mdn mdl mdv]);
# 	hold off;
	
endfunction;



# 	
# 	[p] = normal_discrete_pdf(exp(ml), sqrt((exp(sl^2)-1)*exp(2*ml+sl^2)));
# 	
# 	p = pl + pn;
# 	p = p/sum(p);
	
	
# 	гладкое логнормальное
# 	s = sqrt( sum( v .* (x - m).^2  )/(n-1));
# 	[p] = normal_discrete_pdf(m, s);
# 	printf("m=%0.1f,s=%0.1f\n", m, s);
	
# 	y = [y; p(x)];
# 	ylegend = [ylegend; "chimera"];

# 	# тест
# 	m=ml;s=sl;
# 	d=0.1;
# 	ii=1:d:1000;
# 	p=exp(-0.5*((log(ii)-m)/s).^2)./(ii*s*sqrt(2*pi));
# 	ic = sum(p)*d; 	# 1/c, c — нормировочный коэффициент
# 	p=p/ic;
# 	plot(ii, p)


# a = 7.1301652627783483E+02;
# #        std err squared: 1.14620E+02
# #        t-stat: 6.65993E+01
# #        p-stat: 0.00000E+00
# #        95% confidence intervals: [6.91010E+02, 7.35023E+02]
# b = 1.9189488865515143E+00;
# #        std err squared: 9.30089E-03
# #        t-stat: 1.98976E+01
# #        p-stat: 0.00000E+00
# #        95% confidence intervals: [1.72071E+00, 2.11719E+00]
# c = -3.1753554033192072E-01;
# #        std err squared: 8.00208E-04
# #        t-stat: -1.12251E+01
# #        p-stat: 1.81313E-11
# #        95% confidence intervals: [-3.75682E-01, -2.59389E-01]
# d = -2.2791589799712897E+00;
# #        std err squared: 3.73515E-01
# #        t-stat: -3.72924E+00
# #        p-stat: 9.43395E-04
# #        95% confidence intervals: [-3.53541E+00, -1.02290E+00]
# #y = a * exp(-0.5 * ((ln(x-d)-b)/c)2)
# p = a * exp(-0.5 * ((log(x-d)-b)/c).^2);
# p=p/sum(p);
# 	y = [y; p(x)];
# 	ylegend = [ylegend; "Log-Normal Peak A Shifted 2D"];
	
# # Fitting Interface For Log-Normal 4 Parameter 2D
# # y = a * exp(-1.0 * (ln(2) * ln((((x-b) * (d2-1)) / (c*d)) + 1.0)2) / ln(d)2)	  	Logo
# a = 1.1877387771296890E+02
# #        std err squared: 6.80575E-01
# #        t-stat: 1.43974E+02
# #        p-stat: 0.00000E+00
# #        95% confidence intervals: [1.17078E+02, 1.20470E+02]
# b = 4.5003339620440794E+00
# #        std err squared: 2.70990E-04
# #        t-stat: 2.73381E+02
# #        p-stat: 0.00000E+00
# #        95% confidence intervals: [4.46650E+00, 4.53417E+00]
# c = 2.5379437157357674E+00
# #        std err squared: 4.15595E-04
# #        t-stat: 1.24494E+02
# #        p-stat: 0.00000E+00
# #        95% confidence intervals: [2.49604E+00, 2.57985E+00]
# d = 9.5355386455450719E-01
# #        std err squared: 1.51936E-04
# #        t-stat: 7.73597E+01
# #        p-stat: 0.00000E+00
# #        95% confidence intervals: [9.28217E-01, 9.78891E-01]	
# p = a * exp(-1.0 * (log(2) * log((((x-b) .* (d^2-1)) / (c*d)) + 1.0).^2) / log(d)^2);
# p=p/sum(p);	
# 	y = [y; p(x)];
# 	ylegend = [ylegend; "Log-Normal 4 Parameter 2D"];



# p = a * exp(-1.0 * (log(2) * log((((x-b) .* (d^2-1)) / (c*d)) + 1.0).^2) / log(d)^2);
# Root Mean Squared Error (RMSE): 0.00391118579835

# a = 1.7480356993074406E-01
#        std err squared: 6.88962E-06
#        t-stat: 6.65967E+01
#        p-stat: 0.00000E+00
#        95% confidence intervals: [1.69408E-01, 1.80199E-01]
# b = 4.5346282056349292E+00
#        std err squared: 3.52287E-03
#        t-stat: 7.64000E+01
#        p-stat: 0.00000E+00
#        95% confidence intervals: [4.41262E+00, 4.65663E+00]
# c = 4.3105829694651057E+00
#        std err squared: 5.96636E-03
#        t-stat: 5.58060E+01
#        p-stat: 0.00000E+00
#        95% confidence intervals: [4.15181E+00, 4.46936E+00]
# d = 1.3651769994388694E+00
#        std err squared: 1.43315E-03
#        t-stat: 3.60614E+01
#        p-stat: 0.00000E+00
#        95% confidence intervals: [1.28736E+00, 1.44299E+00]
function y = Peak_LogNormal4Parameter_model(x)
	temp = 0.0;

	% coefficients
	# для общей выборки
# 	a = 1.7480356993074406E-01;
# 	b = 4.5346282056349292E+00;
# 	c = 4.3105829694651057E+00;
# 	d = 1.3651769994388694E+00;
	a = 0.175;
	b = 4.5;
	c = 4.3;
	d = 1.3;

	temp = a * exp(-1.0 * (log(2) * log((((x-b) .* (d^2-1)) / (c*d)) + 1.0).^2) / log(d)^2);

	y = temp;
endfunction;

function SaveLandscape(fig, filename, papersize)
	set (fig, "paperorientation", "landscape");
	set (fig, "papersize", papersize);
	set (fig, "paperposition", [0.25 0.25, papersize-0.5]);
	print(fig, filename);
endfunction;
function SaveLandscapeA4(fig, filename, fontsize)
	if (nargin >= 3)
		set(gca(),"fontsize", fontsize);
	endif;
	
	papersize = [29.7, 21.0]/2.54; 
	SaveLandscape(fig, filename, papersize)
endfunction;function SaveLandscapeA3(fig, filename, fontsize)
	if (nargin >= 3)
		set(gca(),"fontsize", fontsize);
	endif;
	
	papersize = [42, 29.7]/2.54; 
	SaveLandscape(fig, filename, papersize)
endfunction;
