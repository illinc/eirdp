x = 1:100;	# значения длины пути для таблиц



function [r] = random_norm(m,s)
	r = m + s*randn();
endfunction;

function [r] = random_lognorm(m,s)
	r = exp(m + s*randn());
endfunction;


function [d] = distance_X_Px()
# 	d = 0;
	m = random_norm(log(5),0.3);
	s = random_norm(0.3,0.5);
	d = round(random_lognorm(m,s));
endfunction;

function [d] = distance_Px_Py()
	m = log(5);
	s = 0.3;
	d = round(random_lognorm(m,s));
endfunction;

function [d] = distance_Py_Y()
	#m = random_norm(log(5),0.3);
	m = log(5)+0.3*rand();
	#s = random_norm(0.3,0.5);
	s = log(0.3)+0.5*rand(); 
	d = round(random_lognorm(m,s));
# 	d=0;
endfunction;


function [d] = distance_X_Y()
	d = distance_X_Px() + distance_Px_Py() + distance_Py_Y() + distance_Py_Y();
endfunction;



function [d] = distance_by_params(params)
	m = params(1);
	s = params(2);
	d = round(random_lognorm(m,s));
endfunction;


function [dd] = distance_X_Y_2()
	dd = [];
	d = distance_Px_Py();
	
	mx = log(5)+0.3*rand();
	sx = log(0.3)+0.5*rand();
	nx = 10*mx;
	
	my = log(5)+0.3*rand();
	sy = log(0.3)+0.5*rand(); 
	ny = 10*my;
	
	for i = 1:nx
		for j = 1:ny
			dxy = d + distance_by_params([mx sx]) + distance_by_params([my sy]);
			dd = [dd, dxy];
		endfor;
	endfor;
	
endfunction;

function [dd] = distance_X_Y_3(d_Px_Py)
	dd = [];
	
	mx = log(5)+0.3*rand();
	sx = log(0.3)+0.5*rand();
	nx = 10*mx;
	
	my = log(5)+0.3*rand();
	sy = log(0.3)+0.5*rand(); 
	ny = 10*my;
	
	for i = 1:nx
		for j = 1:nx
			dd = [dd, distance_by_params([mx sx])];
		endfor;
	endfor;
	
	for i = 1:nx
		for j = 1:ny
			dxy = d_Px_Py + distance_by_params([mx sx]) + distance_by_params([my sy]);
			dd = [dd, dxy];
		endfor;
	endfor;
	
	for i = 1:ny
		for j = 1:ny
			dd = [dd, distance_by_params([my sy])];
		endfor;
	endfor;	
endfunction;



D = [];

# ND = 40000;
# for i = 1:ND
# 	D = [D, distance_X_Y()];
# endfor;

# for i = 1:1000
# 	D = [D, distance_X_Y_2()];
# endfor;

for i = 1:1
	D = [D, distance_X_Y_3(0)];
	D = [D, distance_X_Y_3(0)];
	D = [D, distance_X_Y_3(8)];
endfor;

HD = histc(D, x);

figure(1, "name", "NumExp");
h = plot(x, HD./sum(HD))






