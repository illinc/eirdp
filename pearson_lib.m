# Чтобы скрипт не начинался с ключевого слова function
ans = 42;

###############################################################################
# 
# Вспомогательные функции
# 
###############################################################################
#==============================================================================
# Определение выборочных характеристик
# v — выборка;
# n — общее количество узлов;
# mli — выборочное среднее логарифма (оценка μ);
# sli — выборочное стандартное отклонение логарифма (оценка σ)
#==============================================================================

# Модосреднее (среднее по моде и её соседям, с коррекцией)
# Коррекция выполняется так, чтобы, 
# если пик и один из его соседей практически равны, модосреднее равнялось их среднему;
# если сильно различаются — модосреднее было близко к пику
function [m, imode] = discrete_mode(frq)
# 	frq = frq/sum(frq);
	[f imode]=max(frq);
	m = imode;	# пик (мода выборки)
	
	# Аппроксимация по пику и двум ближайшим точкам
	try
		
		mm = [imode-1 imode imode+1];	# координаты
		ff = frq(mm);			# частоты
		
# 		# аппроксимация параболой
# 		[p,y,a] = qint(ff(1),ff(2),ff(3));
# 		m = imode+p;	# x-p = 0

		# хитрая подгонка
		ff = ff - min(ff);	# исключение из взвешенной суммы минимального (самого дальнего от моды)
		ff = ff/max(ff);	# нормировка (вес моды (max из тройки) 1, вес min из тройки 0, вес среднего в пределах [0,1])
		ff = ff.^4;	# уменьшение весов, меньших 1
		m = mm*ff'/sum(ff);	# взвешенная сумма 3 точек mm с весами ff
	catch
		printf("Ошибка вычисления модосреднего\n");	
	end_try_catch;
endfunction;
#==============================================================================
# Оценивание двух параметров распределения
#==============================================================================
# двух параметров логарифмически нормального распределения
function [m, s] = logn_params(x, v)
	n = sum(v);
	lni=log(x);	# логарифм ζ, который, если ζ распределена логнормально, распределена нормально
	mli =sum(lni.*v)/n;	# параметр μ, который для ln(ζ) является мат.ожиданием
	dli = sum( v .* (lni - mli).^2  )/(n-1); 
	sli = sqrt(dli);	# параметр σ, который для ln(ζ) является стандартным отклонением
	m=mli;
	s=sli;
endfunction;
# двух параметров нормального распределения
function [m, s] = normal_params(x, v)
	n = sum(v);
	#m =sum(x.*v)/n;	# параметр μ — мат.ожидание
	m = discrete_mode(v);	# параметр μ — мода с коррекцией
	di = sum( v .* (x - m).^2  )/(n-1); 
	s = sqrt(di);	# параметр σ — стандартное отклонение
endfunction;

#==============================================================================
# Расчёт теоретического распределения вероятностей кол-в уровней при заданных μ=m и σ=s
#==============================================================================
# логнормальное распределение
function [p] = logn_discrete_pdf(m, s)
	ii=1:1000;	# большая длина нужна для оценки нормировочного коэффициента
	p=exp(-0.5*((log(ii)-m)/s).^2)./(ii*s*sqrt(2*pi));
	ic = sum(p); 	# 1/c, c — нормировочный коэффициент
	p=p/ic; 	# нормировка
	# p — вероятности для логнормального дискретного с μ=m и σ=s
endfunction;
# нормальное распределение
function [p] = normal_discrete_pdf(m, s)
	ii=1:1000;	# большая длина нужна для оценки нормировочного коэффициента
	p=stdnormal_pdf((ii-m)/s);
	ic = sum(p); 	# 1/c, c — нормировочный коэффициент
	p=p/ic; 	# нормировка
	# p — вероятности для нормального дискретного с μ=m и σ=s
endfunction;

#==============================================================================
# Масштабирование ГОСТовского группирования по m, s
#==============================================================================
# логнормальное распределение
function [g] = logn_discrete_scalegroup(gostgroup, m, s)
	g = exp(gostgroup*s+m);
endfunction;
# нормальное распределение
function [g] = normal_discrete_scalegroup(gostgroup, m, s)
	g = gostgroup*s+m;
endfunction;

#==============================================================================
# Граничные точки интервалов группирования 
# при оценивании двух параметров нормального распределения, логарифмически нормального распределения
# (асимптотически оптимальное группирование)
# k — требуемое количество интервалов
# Источник данных:
# Р 50.1.033-2001 Прикладная статистика. Правила проверки согласия опытного распределения с теоретическим. Часть I. Критерии типа хи-квадрат
#==============================================================================
function [group] = gost(k)
switch (k)
case 3
	group = [	-1.1106	1.1106										];
	# но 3-3=0, не с чем сравнивать
case 4
	group = [	-1.3834	0	1.3834									];
case 5
	group = [	-1.6961	-0.6894	0.6894	1.6961								];
case 6
	group = [	-1.8817	-0.997	0	0.997	1.8817							];
case 7
	group = [	-2.06	-1.2647	-0.4918	0.4918	1.2647	2.06						];
case 8
	group = [	-2.1954	-1.4552	-0.7863	0	0.7863	1.4552	2.1954					];
case 9
	group = [	-2.3188	-1.6218	-1.0223	-0.3828	0.3828	1.0223	1.6218	2.3188				];
case 10
	group = [	-2.4225	-1.7578	-1.2046	-0.6497	0	0.6497	1.2046	1.7578	2.4225			];
case 11
	group = [	-2.5167	-1.8784	-1.3602	-0.8621	-0.3143	0.3143	0.8621	1.3602	1.8784	2.5167		];
case 12
	group = [	-2.5993	-1.9028	-1.4914	-1.0331	-0.5334	0	0.5334	1.0331	1.4914	1.9028	2.5993	];
case 13
	group = [	-2.6746	-2.0762	-1.6068	-1.1784	-0.7465	-0.2669	0.2669	0.7465	1.1784	1.6068	2.0762	2.6746];
case 14
	group = [	-2.7436	-2.1609	-1.7092	-1.3042	-0.9065	-0.4818	0	0.4818	0.9065	1.3042	1.7092	2.1609	2.7436];
case 15	
	group = [	-2.8069	-2.2378	-1.8011	-1.415	-1.0435	-0.659	-0.2325	0.2325	0.659	1.0435	1.415	1.8011	2.2378	2.8069];
otherwise
    group=[];
endswitch
endfunction;
#==============================================================================
# q — квантиль α=0.05 (95%) для количества степеней свободы r
# r=k-m-1=k-3 (эта квантиль меньше, чем для k-1)
# Источник данных:
# Р 50.1.033-2001 Прикладная статистика. Правила проверки согласия опытного распределения с теоретическим. Часть I. Критерии типа хи-квадрат
#==============================================================================
function [q] = ChiSquareQuantile95(r)
qq = [3.841
	5.991
	7.815
	9.488
	11.07
	12.592
	14.067
	15.507
	16.919
	18.307
	19.675
	21.026
	22.362
	23.685
	24.996
	26.296];
	try
		q = qq(r);
	catch
		q = -1;
	end_try_catch;
endfunction;
#==============================================================================
function [k] = k_VNIIM(n)
	if (n < 40)
		k = [-1 -1];
	elseif	(n < 100)
		k = [7 9];
	elseif	(n < 500)
		k = [8 12];
	elseif	(n < 1000)
		k = [10 16];
	elseif	(n < 10000)
		k = [12 22];
	else
		k = [-1 -1];	
	endif;
endfunction;#==============================================================================
function [group] = equal_p(k, p)
	group = [];
	groups=1;
	s = 0;
	np = length(p);
	for i=1:(np-1)
		s = s + p(i);
		if (s > groups/k)
			#group = [group i+1];	# граница включается в верхний интервал <- так очень большим получается первый интервал
			group = [group i];
			groups = groups + 1;
		endif;
	endfor;
endfunction;

#==============================================================================
# Вывод массива
function printarray(name, value)
	printf("%16s = ", name);
	printf("%8.2f ", value)
	printf("\n");
endfunction;
# Вывод массива в указанном формате
function printfarray(fmt, name, value, endmark)
	printf("%16s = ", name);
	printf(fmt, value)
	if (nargin < 4)
		printf("\n");
	else
		printf("%s", endmark);
	endif;
endfunction;
#==============================================================================

#==============================================================================
# Группирование
#==============================================================================
# groupmax — максимальные значения всех групп, кроме последней (от ... до ∞)
# groupmax д. б. целыми !!!
# т.е. 1-я группа — от 1 до groupmax(1)
# группы с 2 до предпоследней — от groupmax(i-1)+1 до groupmax(i)
# последняя — от groupmax(последнего)+1 до ∞
# function [Ygrouped] = mkgroup(Y, groupmax, Ysum)
# 	gmin = 1;
# 	g = [];
# 	for gmax=groupmax
# 		g = [g sum(Y(gmin:gmax))];
# 		gmin = gmax+1;
# 	endfor;
# 	g = [g Ysum-sum(g)];
# 	Ygrouped = g;
# endfunction;
#==============================================================================
#==============================================================================
# bounds — верхние границы всех групп, кроме последней (от ... до ∞)
# bounds — любые
# т.е. 1-я группа — x ≤ bounds(1)
# группы с 2 до предпоследней — bounds(i-1) < x ≤ bounds(i)
# последняя — от x > bounds(последнего) [т. е. все остальные]
# (границу включаем в нижний интервал)
# function [Ygrouped] = mkgroup2(Y, bounds, Ysum)
# 	x=1:length(Y);
# 	idx =  x<=bounds(1);
# 	g = sum(Y(idx));
# 		
# 	ng = length(bounds);
# 	for i=2:ng
# 		idx =  (x>bounds(i-1))&(x<=bounds(i));
# 		g = [g sum(Y(idx))];
# 	endfor;
# 	g = [g Ysum-sum(g)];
# 	Ygrouped = g;
# endfunction;
# то же, но границу включаем в верхний интервал (0 считаем положительным числом)
function [Ygrouped] = mkgroup3(Y, bounds, Ysum)
	x=1:length(Y);
	idx =  x<bounds(1);
	g = sum(Y(idx));
		
	ng = length(bounds);
	for i=2:ng
		idx =  (x>=bounds(i-1))&(x<bounds(i));
		g = [g sum(Y(idx))];
	endfor;
	g = [g Ysum-sum(g)];
	Ygrouped = g;
endfunction;
#==============================================================================

#==============================================================================
# Расчёт статистики хи-квадрат (hh) 
# по выборке v, теоретической вероятности p, 
# правым границам групп groupmax — каким есть
# и сравнение с соответствующей квантилью (q, на экране)
#==============================================================================
function [hh, h, pgn, summary] = CalcChiSquare(p, v, groupbounds, verbose)
	n = sum(v);
	
	if (verbose) printarray("bounds", groupbounds); endif;
	# вероятность попадания в интервалы
	pg = mkgroup3(p, groupbounds, 1);
	
	# теоретическая частота попадания в интервалы
	pgn=pg*n;
	if (verbose) printarray("pg*n", pgn); endif;
	
	
	# выборочная частота попадания в интервалы
	vg = mkgroup3(v, groupbounds, n);
	if (verbose) printarray("vg", vg); endif;
	
	# слагаемые статистики хи-квадрат поинтервально
	h=(vg - pgn).^2./pgn;
	# Вывод слагаемых χ2, чтобы было видно плохие (с большим значением) интервалы
	if (verbose) printarray("h", h); endif;
	
	# статистика хи-квадрат
	hh = sum(h);
	# hh = sumskipnan(h);	# пропуск интервалов с 0 вероятностью — нет, таких не должно быть
	
	k = length(vg);	# количество групп (интервалов)
	q = ChiSquareQuantile95(k-3);	# k интервалов - 2 оцениваемых параметра [μ, σ] - 1
	q2 = ChiSquareQuantile95(k-1);	# k интервалов - 1
	# Мы можем уверенно (с 95% вероятностью) отвергать нулевую гипотезу, eсли hh > q2;
	# нет причин отвергать нулевую гипотезу, если hh < q;
	# а если q ≤ hh ≤ q2, то вообще ничего утверждать нельзя
# 	printf("Sχ2 = %0.2f < χ2(%d-3,α) = %0.2f\n?\n", hh, k, q);
	if (verbose)
		printf("Sχ2 = %0.2f < χ2(%d-3,α) < χ2(%d-1,α)   <=>   %0.2f  < %0.2f < %0.2f \n", hh, k, k, hh, q, q2);
	endif;
	#		good	bad	total
	summary =	[(hh<q)	(hh>q2)	1];
endfunction;

#==============================================================================
# Расчёт статистики хи-квадрат (hh) 
# по выборке v (здесь выборка = гистограмма частот, а не длиннющий набор значений!!!), 
# теоретической вероятности p, 
# правым границам групп groupmax — с коррекцией интервалов
#==============================================================================
function [hh, h, summary] = ChiSquare(p, v, groupbounds, verbose)
	if (length(groupbounds) >= length(v))
		printf("Выборку из %d уровней нельзя разбить на %d интервалов", length(v), length(groupbounds)+1);
	endif
	summary = [0 0 0];
	
	# вероятность попадания в интервалы
# 	pg = mkgroup(p, groupmax, 1);
	pg = mkgroup3(p, groupbounds, 1);
	
	# Удаление интервалов с 0 вероятностью
	idxnul = pg==0;
	while (sum(idxnul)>0)
		if (verbose)
			printarray("bounds", groupbounds);
			printf("Объединение интервалов с нулевой вероятностью\n");
		endif;
		# со следующими за ними, так как последний интервал точно имеет ненулевую вероятность
		groupbounds(idxnul)=[];
		# printarray("bounds", groupbounds);
		pg = mkgroup3(p, groupbounds, 1);
		idxnul = pg==0;
	endwhile;
		
	[hh, h, pgn, smm_gr] = CalcChiSquare(p, v, groupbounds, verbose);
	summary = summary + smm_gr;
	
	# Объединение интервалов с частотой < 5 --
	minf = 5;
	# Последний интервал объединяем с предыдущим
	lastgroup = length(pgn);
	if (pgn(lastgroup)<minf)
		if (verbose) 	printf("Объединение интервалов с ожидаемой частотой < %d\n", minf); endif;
		groupbounds(lastgroup-1)=[];
		[hh, h, pgn, smm_gr] = CalcChiSquare(p, v, groupbounds, verbose);
		summary = summary + smm_gr;
	endif;
	# Остальные — со следующими
	i = 1;
	while (i < length(pgn)) # последний так не считается
		if (pgn(i)<minf)
			if (verbose) printf("Объединение интервалов с ожидаемой частотой < %d\n", minf); endif;
			groupbounds(i)=[];
			[hh, h, pgn, smm_gr] = CalcChiSquare(p, v, groupbounds, verbose);
			summary = summary + smm_gr;
			
			idxnul = pgn<minf;
			# если groupbounds укоротили, то под номером i уже другой интервал,
		else	# а если нет —
			# то первый непроверенный интервал — (i+1)-й
			i = i+1;
		endif;
		
	endwhile;
	#-----------------------------------------
	
	if (verbose) printf("\n"); endif;
endfunction;

# Проверка по критерию хи-квадрат

# По ГОСТовским интервалам
function [hh, h, summary] = ChiSquareGost(x, p, v, k, s, m, verbose)
	[gostgroup] = gost(k);
	summary = [0 0 0];
	
	group=scalegroup(gostgroup, m, s);
	
# 	printarray("гр.точки (неокр)", group);
	[hh, h, smm] = ChiSquare(p, v, group, verbose);
	summary = summary + smm;
	
# 	printf("Усечение вниз\n");
# 	[hh, h] = ChiSquare(p, v, unique(floor(group)));
# 	printf("Округление к ближайшему\n");
# 	[hh, h] = ChiSquare(p, v, unique(round(group)));
# 	printf("Усечение вверх\n");
# 	[hh, h] = ChiSquare(p, v, unique(ceil(group)));
endfunction;
function [summary] = ChiSquare_alltest(v, krange, verbose)
	summary = [0 0 0];

	# n = sum(v);
	lvl=length(v); # количество различных значений случайной величины ζ (кол-ва уровней в выборке)
	x=1:lvl;	# возможные значения случайной величины ζ
	if (verbose) printfarray("%2d ", "x", x); endif;
	if (verbose) printfarray("%2d ", "v", v); endif;
	
	[m, s] = pdf_params(x, v);
	if (verbose) printf("Выборочные: μ=%g и σ=%g; μ+3σ=%g\n", m, s, m+3*s); endif;
	
	#==============================================================================
	# Расчёт теоретического распределения вероятностей кол-в уровней при заданных μ=m и σ=s
	p = probability_density_function(m, s);
	#==============================================================================
	
	for k=krange
		try
			if (verbose) printf("Асимптотически оптимальное группирование k=%d по ГОСТ для непрерывных нормального и логнормального\n", k); endif;
			[hh, h, smm] = ChiSquareGost(x, p, v, k, s, m, verbose);
			summary = summary + smm;
		catch
			if (verbose) printf("...%s\n", "Сбой"); endif;
		end_try_catch;
		try
			if (verbose) printf("Группы с равной вероятностью (k=%d)\n", k); endif;
			[hh, h, smm] = ChiSquare(p, v, equal_p(k, p), verbose);
			summary = summary + smm;
		catch
			if (verbose) printf("...%s\n", "Сбой"); endif;
		end_try_catch;
	endfor;
	if (verbose) printf("***********************************************************************\n"); endif;
endfunction;
