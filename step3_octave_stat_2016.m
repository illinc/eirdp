# clear;
# clc;
iptest_all_lib;


function savecolumns(z, filename, fileheader)
  save(filename, "fileheader");
  save("-append", filename, "z");
endfunction;

# close("all")	# закрываем все открытые окна с графиками

# x = 1:40;	# возможные значения длины пути
x = 1:30;	# значения длины пути для таблиц

function [col_id] = filename2columnid(filename)
# 	col_id = strrep(filename, "-all-octet-", "-");
	col_id = filename;
	col_id = strrep(col_id, "/", "-");
	col_id = strrep(col_id, "s-", "");
	col_id = strrep(col_id, ".dat", "");
	
	col_id = strrep(col_id, "aotech.ru", "aotech");
	col_id = strrep(col_id, "open-anime.org", "aotech");
	
	col_id = strrep(col_id, "lexie-heavy-mobile-pc", "heavy");
	
	col_id = strrep(col_id, "dbs1.russkayamoda.ru", "russkayamoda");
	
	
	col_id = strrep(col_id, "rutracker-sortlist", "rutracker");
endfunction;

legendstrings = {"avg (1)"};
filecolumns = " 0. x";
fileheader = 'x';

W = [];
U = [];

#Samples = [];
Measures = [];

filelist = ls('s-*/*-*.dat');
filescount = size(filelist)(1);
for filenum = 1:filescount
  filename = strtrim(filelist(filenum,:));
  Z = load(filename);
  
  u = reshape(histc(Z(:,2)', x), 1, length(x));	# частоты длин пути без коррекции
  v = reshape(histc(Z(:,3)', x), 1, length(x));	# частоты длин пути с коррекцией
  if (size(x) == size(v) )
    U =[U u'];
    W =[W v'];
    
    col_id = filename2columnid(filename);

    legendstrings = [legendstrings, {sprintf("%s (%d)", col_id, filenum+1)}];
    filecolumns = [filecolumns; sprintf("%2d. %s", filenum, col_id)];
    fileheader = sprintf("%s %s", fileheader, col_id);
    
    #Samples = [Samples Z(:,3)];	# Выборка длин пути с коррекцией
    # они разной длины
    
    corrSample = Z(:,3);
    corrmode	= mode(corrSample);
    corrmean	= mean(corrSample);
    corrstd	= std(corrSample);
    corrskewness	= skewness(corrSample);
    corrkurtosis	= kurtosis(corrSample) - 3;
 
    Measures = [Measures; corrmode corrmean corrstd corrskewness corrkurtosis];
    

  endif;
endfor;
# Measures
savecolumns([Measures; mean(Measures)], 'corr-measures.mat', "mode mean std skewness excesskurtosis");

# Q = U./sum(U);	# нормированное распределение пути без коррекции
# J = W./sum(W);	# нормированное распределение пути с коррекцией
# CJ = cumsum(J);
# 
# figure(1, "name", "Corr");
# h = plot(x, [mean(J')' J]);
# legend( legendstrings );
# sel(1, h);
# 
# figure(2, "name", "Uncorr");
# g = plot(x, [0*x' Q]);
# legend( legendstrings );
# sel(1, g);
# 
# figure(3, "name", "Corr-cumsum");
# h = plot(x, [mean(CJ')' CJ]);
# legend(legendstrings);
# sel(1, h);
# 
# 
# function savecolumns(z, filename, fileheader)
#   save(filename, "fileheader");
#   save("-append", filename, "z");
# endfunction;
# 
# fileheader = sprintf("%s %s %s %s", fileheader, "statistics-minus", "statistics-mean", "statistics-plus");
# 
# function [z] = stat2file(x, V)
#   avgV = mean(V')';
#   
# #   # экспериментальный 95% интервал
# #   ncol =  size(V,2)	# общее количество измерений
# #   nmarg=(1-0.95)*ncol	# количество отбрасываемых крайних значений (всего, а не с каждого края)
# #   sV = sort(V,2)	# сортировка значений каждой точки по отдельности (в sV колонка больше не соответствует одному измерению, зато каждая строка отсортирована, и выбросы находятся в первых и последних колонках)
# # ^^^ не считается, так как мало измерений
# 
#   z = [x',  V,  -(min(V')'-avgV),  avgV,  max(V')'-avgV];
# endfunction;
# 
# savecolumns(stat2file(x, U), 'uncorr.mat', fileheader);
# savecolumns(stat2file(x, Q), 'uncorr-norm.mat', fileheader);
# savecolumns(stat2file(x, W), 'corr.mat', fileheader);
# savecolumns(stat2file(x, J), 'corr-norm.mat', fileheader);
# 
# savecolumns(stat2file(x, CJ), 'corr-norm-cumsum.mat', fileheader);
# 
# disp(filecolumns);
