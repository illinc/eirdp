# clear;
# clc;
iptest_all_lib;

close("all")	# закрываем все открытые окна с графиками

x = 1:30;	# возможные значения длины пути

isverbose = 0;	# 0 — тест хи-квадрат без подробного вывода, 1 — с оным
Z = load(sprintf("%s/%s-all-octet.dat", sampleid, nodeid));	# исходные данные в 3 столбца "<1-й октет IP> <путь> <путь без узлов провайдера>"
octetunique = unique(Z(:,1));	# Различные первые октеты

# Для выделения больших (>=threshold_n) групп с одним первым октетом
# threshold_n = 40;
threshold_n = 200;

# Какая должна быть доля результатов «за» H0, чтобы, при отсутствии результатов «против», посчитать такой результат заслуживаюшим доверия
threshold_chi2resultsH0 = 0.7;	

# min и max кол-во разбиений
# (ГОСТовские рекомендации для непрерывного случая, а у нас дискретный)
# k = [3 16];
k = [5 8];

# Для графиков v(x) [y(x)]: 
# * — нормированное распределение / *nn — ненормированное распределение / *legend — подпись для легенды
y = ynn = ylegend = [];	# все большие группы IP (с n>=threshold_n)
good = goodnn = goodlegend = [];	# большие (с n>=threshold_n) логнормальные группы IP
bad = badnn = badlegend = [];	# большие (с n>=threshold_n) нелогнормальные группы IP
H0_norm = H0_nn = H0_legend = [];	# группы с достоверной на 95% H0 (т. е. без «против» и с малым количеством неопределённых результатов — наименьшая доля «за» threshold_chi2resultsH0)
vsingletons = 0*x;	# одна выборка — совокупность «групп» из одного IP (n==1)


printf("Группировка по первым октетам и расчёт, подождите...\n");
fflush(stdout);
# Получение мини-выборок (по 1-м октетам IP)
# Для очень малых выборок возникает деление на 0, поэтому отключаем это сообщение
warning ("off", "Octave:divide-by-zero");
octetsample=[]; 
# n1_lines = [];
for oc=octetunique',
# 	n1_lines = [n1_lines, find(Z(:,1)==oc)(1)];	# для каждого октета запоминаем номер первой строки с таким октетом (нужно только для создания выборки n1, в которую входит ровно 1 путь из каждой мини-выборки)

	v = oneoctet(oc, x, Z);	# выборка с IP = oc.x.x.x (v(i) — количество путей длины i в выборке)
	
	# тестирование выборки v по хи-квадрат с различными разбиениями в диапазоне k
	# n — кол-во узлов, m — параметр сдвига, s — масштабный, summary — статистика по тестам [за/против/всего]
	[n, m, s, summary] = full_test(v, x, k, isverbose);
		
	# Большие группы сохраняем особо
	if (n >= threshold_n)
		vtitle = sprintf("%3d", oc);
		[y, ynn, ylegend] = adddata(v, vtitle, n, m, s, summary, x, y, ynn, ylegend);
		if (summary(1)>summary(2))	# за > против
			[good, goodnn, goodlegend] = adddata(v, vtitle, n, m, s, summary, x, good, goodnn, goodlegend);
		else
			[bad, badnn, badlegend] = adddata(v, vtitle, n, m, s, summary, x, bad, badnn, badlegend);
		endif;
		
		if ((summary(2)==0) & (summary(1)/summary(3) > threshold_chi2resultsH0))	# без «против» и с не менее 90% «за» H0
			[H0_norm, H0_nn, H0_legend] = adddata(v, vtitle, n, m, s, summary, x, H0_norm, H0_nn, H0_legend);
		endif;
	endif;
	
	# Группы из одного пути объединяем в выборку vsingletons
	if (n == 1)
		vsingletons = vsingletons + v;
	endif;	
	
	# Данные по группе сохраняются в массив
	octetsample = [octetsample;
		oc n m s k summary
	];
endfor;
# Все возможные пути
[y, ynn, ylegend, all_idx] = test_and_adddata(histip(x, Z(:,2:3)), "all", x, y, ynn, ylegend, k, isverbose);
# Объединение больших логнормальных выборок — не логнормально, так как у распределений разные параметры
clear good_sum_idx;
if (size(good,1) > 1)
	[good, goodnn, goodlegend, good_sum_idx] = test_and_adddata(sum(goodnn), "sum", x, good, goodnn, goodlegend, k, isverbose);
# [good, goodnn, goodlegend, good_avg_idx] = test_and_adddata(mean(good), "avg", x, good, goodnn, goodlegend, k, isverbose);
endif;
# Объединение больших нелогнормальных выборок — тем более
clear bad_sum_idx;
if (size(bad,1) > 1)
	[bad, badnn, badlegend, bad_sum_idx] = test_and_adddata(sum(badnn), "sum", x, bad, badnn, badlegend, k, isverbose);
	# [bad, badnn, badlegend, bad_avg_idx] = test_and_adddata(mean(bad), "avg", x, bad, badnn, badlegend, k, isverbose);
endif;

warning ("on", "Octave:divide-by-zero");
printf("Закончен.\n");

octetsample_by_m = sortrows(octetsample, 3);	# сортируем строки по μ

threshold_n_y = 3;
# printf("Вывести «чистые» группы по октетам (y/N/f)?\ny — да, но без групп размера <=%d, n — нет, f — все.\n", threshold_n_y);
# fflush(stdout);
# userans = kbhit();	# неудобно
userans="y";

if (userans=="y" | userans=="f")
# 	printf("Группы по первым октетам в порядке возрастания параметра сдвига\n");
# 	show_ip_groups(octetsample_by_m, userans=="f", threshold_n_y);
	printf("Группы по первым октетам в порядке убывания размера выборки\n");
	show_ip_groups(sortrows(octetsample, -2), userans=="f", threshold_n_y);
endif;

# fflush(stdout);
# Графики всех больших выборок по IP и жирным — выборки, включающей все измерения 
if (size(y,1) > 0)
	figure(21, "name", sprintf("All IP: n>=%d", threshold_n));
	h1 = plot(x, y);
	axis([0,max(x), 0, max(max(y))]);
	legend(ylegend);
	set (h1(all_idx), "linewidth", 4) 
endif;

# Большие логнормальные
if (size(good,1) > 0)
	figure(23, "name", "Lognormal IP (H0 > H1)");
	hgood = plot(x, good);
	axis([0,max(x), 0, max(max(good))]);
	legend(goodlegend);
	if (exist("good_sum_idx", "var") == 1) set(hgood(good_sum_idx), "linewidth", 4); endif;
	# set(hgood(good_avg_idx), "linewidth", 2);
endif;
	
# printf("«Чистые» логнормальные группы, n >= %d\n", threshold_n);
# print_data(good, goodnn, goodlegend);

# Большие нелогнормальные
if (size(bad,1) > 0)
	figure(24, "name", "Non-lognormal IP (H0 <= H1)");
	hbad = plot(x, bad);
	axis([0,max(x), 0, max(max(bad))]);
	legend(badlegend);
	if (exist("bad_sum_idx", "var") == 1) set(hbad(bad_sum_idx), "linewidth", 4); endif; 
	# set(hbad(bad_avg_idx), "linewidth", 2) 
endif;


# mix = mixnn = mixlegend = [];
# 
# # Смешанные группы
# mixoctg = {
# [89 92            ],	# μ=1.5
# [31 79 81 217],	# μ=1.6
# [37 77 176],
# [93 80 78 194 90 88 213],	# μ=1.6 (крупные группы)
# [79 81 217 31  37 77 176]
# };
# # octetsamplemix=[]; 
# for i=1:length(mixoctg)
# 	octets = mixoctg{i};
# 	v = multioctet(octets, x, Z);	# выборка с IP = oc.x.x.x (v(i) — количество путей длины i в выборке)
# 	vtitle = strtrim(sprintf("%d ", octets));
# 	
# 	# тестирование выборки v по хи-квадрат с различными разбиениями в диапазоне k
# 	# n — кол-во узлов, m — параметр сдвига, s — масштабный, summary — статистика по тестам [за/против/всего]
# 	[n, m, s, summary] = full_test(v, x, k, isverbose);
# 		
# 	[mix, mixnn, mixlegend] = adddata(v, vtitle, n, m, s, summary, x, mix, mixnn, mixlegend);
# 	
# 	if ((summary(2)==0) & (summary(1)/summary(3) > threshold_chi2resultsH0))	# без «против» и с не менее 90% «за» H0
# 		[H0_norm, H0_nn, H0_legend] = adddata(v, vtitle, n, m, s, summary, x, H0_norm, H0_nn, H0_legend);
# 	endif;
# 	
# # 	# Данные по группе сохраняются в массив
# # 	oc_key = octets(1)+ octets(length(octets))/1000;
# # 	octetsamplemix = [octetsamplemix;
# # 		oc_key n m s k summary
# # 	];
# endfor;
# # octetsamplemix_by_m = sortrows(octetsamplemix, 3);	# сортируем строки по μ
# # printf("Смешанные группы в порядке возрастания параметра сдвига\n");
# # printf("Октет        n  μ  σ  k   %3s/%3s/%3s\n", "H0", "H1", "всего")
# # for i=1:size(octetsamplemix_by_m,1)
# # 	sample=octetsamplemix_by_m(i,:);
# # 	printf("ip=%6.3d, n=%3d, μ=%0.1f, σ=%0.1f  k=%2d-%2d  %3d/%3d/%3d\n", sample);
# # endfor;
# # [mix, mixnn, mixlegend] = multioctet_add_test([85 87 62          ], x, mix, mixnn, mixlegend, Z, k, isverbose);	# μ=1.8: не логнорм
# # [mix, mixnn, mixlegend] = multioctet_add_test([195  212          ], x, mix, mixnn, mixlegend, Z, k, isverbose);	# μ=1.9: мало узлов
# # # [mix, mixnn, mixlegend] = multioctet_add_test([82            ], x, mix, mixnn, mixlegend, Z, k, isverbose);	# μ=2.0: очень мало узлов
# # [mix, mixnn, mixlegend] = multioctet_add_test([136 86           ], x, mix, mixnn, mixlegend, Z, k, isverbose);	# μ=2.1: мало узлов
# # [mix, mixnn, mixlegend] = multioctet_add_test([            ], x, mix, mixnn, mixlegend, Z, k, isverbose);	# 
# # [mix, mixnn, mixlegend] = multioctet_add_test([            ], x, mix, mixnn, mixlegend, Z, k, isverbose);	# 
# # [mix, mixnn, mixlegend] = multioctet_add_test([            ], x, mix, mixnn, mixlegend, Z, k, isverbose);	# 
# 
# [mix, mixnn, mixlegend, sum_idx] = test_and_adddata(sum(mixnn), "sum", x, mix, mixnn, mixlegend, k, isverbose);
# 
# # Повторы — после суммы, чтобы не учитывать их в оной
# # [mix, mixnn, mixlegend] = multioctet_add_test([79 81 217 31  37 77 176], x, mix, mixnn, mixlegend, Z, k, isverbose);	# μ=1.6 все
# # Выборка из путей с уникальным первым октетом IP
# [mix, mixnn, mixlegend] = test_and_adddata(vsingletons, "vsingletons", x, mix, mixnn, mixlegend, k, isverbose);
# # # Выборка, в которую входит один (первый) путь из каждой группы по первым октетам IP
# # [mix, mixnn, mixlegend, n1_idx] = test_and_adddata(histip(x, Z(n1_lines,2:3)), "n1", x, mix, mixnn, mixlegend, k, isverbose);
# 
# if (size(mix,1) > 0)
# 	figure(22, "name", "Mix lognormal");
# 	hmix = plot(x, mix);
# 	axis([0,max(x), 0, max(max(mix))]);
# 	legend(mixlegend);
# 	set(hmix(sum_idx), "linewidth", 4);
# 	# set(hmix(n1_idx), "linewidth", 2) 
# 	# printf("Смешанные группы\n");
# 	# print_data(mix, mixnn, mixlegend);
# endif;

if (size(H0_norm,1) > 0)
	clear H0_sum_idx;
	if (size(H0_norm,1) > 1)
		sumH0=sum(H0_nn);
		avgH0=mean(H0_norm);
		[H0_norm, H0_nn, H0_legend, H0_sum_idx] = test_and_adddata(sumH0, "sum", x, H0_norm, H0_nn, H0_legend, k, isverbose);
	endif;
	# [H0_norm, H0_nn, H0_legend, H0_avg_idx] = test_and_adddata(avgH0*sum(sumH0), "avg", x, H0_norm, H0_nn, H0_legend, k, isverbose);
	figure(26, "name", sprintf("H0 (H1 = 0, H0/all > %g)", threshold_chi2resultsH0));
	hH0 = plot(x, H0_norm);
	axis([0,max(x), 0, max(max(H0_norm))]);
	legend(H0_legend);
	if (exist("H0_sum_idx", "var") == 1) set(hH0(H0_sum_idx), "linewidth", 3); endif;
	# set(hH0(H0_avg_idx), "linewidth", 2);
	printf("Выборки, где гипотеза H0 не отвергается ни при каком разбиении\n");
	print_data(H0_norm, H0_nn, H0_legend);
endif;

# dis_norm = dis_nn = dis_legend = [];
# disoctg = {
# [78],
# [79],
# [128],
# # [31 79 81 217],
# # [37 77 176]	
# };
# for i=1:length(disoctg)
# 	octets = disoctg{i};
# 	[dis_norm, dis_nn, dis_legend] = multioctet_add_test(octets, x, dis_norm, dis_nn, dis_legend, Z, k, isverbose);
# endfor;
# 
# figure(25, "name", "To disser");
# hdis = plot(x, dis_norm);
# axis([0,max(x), 0, max(max(dis_norm))]);
# legend(dis_legend);
# 
# printf("В диссер\n");
# print_data(dis_norm, dis_nn, dis_legend);
fflush(stdout);

# printf("Большие выборки\n");
figure(27, "name", "Big groups");
R=C=4;
big_octets = sortrows(octetsample, -2)(1:(R*C));	# первые R*C октетов octetsample, отсортированного по 2 столбцу n в порядке убывания
for i = 1:(R*C)
	subplot(R,C,i);
	hold on;
	oc = big_octets(i);
	v = oneoctet(oc, x, Z);
	n = sum(v);
# 	printf("ip=%3d.x.x.x, n=%3d ", oc, n);
	
	# Локальные максимумы
	locmax = findmaxima(v)';
	h = vstem(locmax, v,  "-g");
	set(h, "marker", "none");
# 	printfarray("%2d ", "Лок.макс.", locmax, sprintf("%*s", 3*(8-length(locmax)), " "));
	
	# Локальные минимумы
	locmin = findminima(v)';
	h = vstem(locmin, v,  "-b");
	set(h, "marker", "none");
# 	printfarray("%2d ", "Лок.мин.", locmin, "\n");
	
	h = plot(x, v, "-r");
	set(h, "linewidth", 3);
# 	legend(["max"; "min"; sprintf("%d\nn=%d", oc, sum(v))]);
# 	legend right;
# 	title(sprintf("%d, n=%d", oc, sum(v)));
# 	xlabel(sprintf("%d, n=%d", oc, sum(v)));
	text(max(x)*0.6, max(v)*0.8, sprintf("%d.x.x.x\nn=%d", oc, n));
	hold off;
endfor;

figure(28, "name", "All IP and fit");
v=histip(x, Z(:,2:3));
[p]=fit_and_show(v, x);


