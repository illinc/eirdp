# clear;
# clc;
iptest_all_lib;


function savecolumns(z, filename, fileheader)
  save(filename, "fileheader");
  save("-append", filename, "z");
endfunction;

# close("all")	# закрываем все открытые окна с графиками

# x = 1:40;	# возможные значения длины пути
x = 1:30;	# значения длины пути для таблиц

function [col_id] = filename2columnid(filename)
# 	col_id = strrep(filename, "-all-octet-", "-");
	col_id = filename;
	col_id = strrep(col_id, "/", "-");
	col_id = strrep(col_id, "s-", "");
	col_id = strrep(col_id, ".dat", "");
	
	col_id = strrep(col_id, "aotech.ru", "aotech");
	col_id = strrep(col_id, "open-anime.org", "aotech");
	
	col_id = strrep(col_id, "lexie-heavy-mobile-pc", "heavy");
	
	col_id = strrep(col_id, "dbs1.russkayamoda.ru", "russkayamoda");
	
	
	col_id = strrep(col_id, "rutracker-sortlist", "rutracker");
endfunction;

legendstrings = {"avg (1)"};
filecolumns = " 0. x";
fileheader = 'x';



# # хи-квадрат по измерениям набор-корень в разное время
# filelist = ls('s-p*/a*-*.dat');
# filescount = size(filelist)(1);
# 
# #   filename = strtrim(filelist(1,:));
#   filename = strtrim(filelist(2,:));
#   Z = load(filename);
#   v = reshape(histc(Z(:,3)', x), 1, length(x));	# частоты длин пути с коррекцией
#   p = v./sum(v);	# нормированное распределение пути без коррекции
# 
#   k = 11;
#   verbose = 1;
#   
# # for filenum = 2:filescount
# for filenum = 3:filescount
#   filename = strtrim(filelist(filenum,:));
#   Z = load(filename);
#   
#   v = reshape(histc(Z(:,3)', x), 1, length(x));	# частоты длин пути с коррекцией
#     
#   
# 	[hh, h, smm] = ChiSquare(p, v, equal_p(k, p), verbose);
# 
# endfor;
# # конец хи-квадрат по измерениям набор-корень в разное время  

W = [];
U = [];

#Samples = [];
Measures = [];

filelist = ls('s-*/*-*.dat');
# filelist = ls('s-p*/m*-*.dat');
filescount = size(filelist)(1);
for filenum = 1:filescount
# for filenum = 27
  filename = strtrim(filelist(filenum,:))
  Z = load(filename);
  
  u = reshape(histc(Z(:,2)', x), 1, length(x));	# частоты длин пути без коррекции
  v = reshape(histc(Z(:,3)', x), 1, length(x));	# частоты длин пути с коррекцией
  if (size(x) == size(v) )
    U =[U u'];
    W =[W v'];
    
    col_id = filename2columnid(filename);

    legendstrings = [legendstrings, {sprintf("%s (%d)", col_id, filenum+1)}];
    filecolumns = [filecolumns; sprintf("%2d. %s", filenum, col_id)];
    fileheader = sprintf("%s %s", fileheader, col_id);
    
    #Samples = [Samples Z(:,3)];	# Выборка длин пути с коррекцией
    # они разной длины
    
    zSample = Z(:,3); # без провайдера / с коррекцией
#     zSample = Z(:,2); # с провайдером / без коррекции
#     zWithZeros = Z(:,3);
#     zWithoutZeros = zWithZeros(zWithZeros>0);
#     size(zWithoutZeros)
#     zSample = log(  zWithoutZeros  );

#    v = reshape(histc(zSample', log(x)), 1, length(x));	# частоты длин пути с коррекцией
    zmode	= mode(zSample);
    zmean	= mean(zSample);
    zstd	= std(zSample);
    zskewness	= skewness(zSample);
    zkurtosis	= kurtosis(zSample) - 3;
 
    Measures = [Measures;  zmode zmean zstd zskewness zkurtosis];
    

  endif;
endfor;
# Measures
# savecolumns([Measures; mean(Measures)], 'corr-measures.mat', "mode mean std skewness excesskurtosis");
# savecolumns([Measures; mean(Measures)], 'corr-log-measures.mat', "mode mean std skewness excesskurtosis");
# savecolumns([Measures; mean(Measures)], 'stattables/uncorr_moments_by_id.mat', "mode mean std skewness excesskurtosis");

# Q = U./sum(U);	# нормированное распределение пути без коррекции
# J = W./sum(W);	# нормированное распределение пути с коррекцией



# function savemomentscatter(midx, Measures, filename, rowheader)
#   momentvalues = Measures(:,midx);
#   momentscatter = [min(momentvalues) quantile(momentvalues,0.25) median(momentvalues)  mean(momentvalues) quantile(momentvalues,0.75) max(momentvalues)];
#   save("-ascii", filename, "rowheader");
#   save("-append", filename, "momentscatter");
# endfunction;
# 
# savemomentscatter(1, Measures, 'stattables/uncorr-momentscatter-by-mmsae.mat', 'mode')

allMomentsScatter = [];
#  zmode zmean zstd zasimm zex
for midx = 1:5
  momentvalues = Measures(:,midx);
  momentscatter = [min(momentvalues) quantile(momentvalues,0.25) median(momentvalues)  mean(momentvalues) quantile(momentvalues,0.75) max(momentvalues)];
  allMomentsScatter = [allMomentsScatter;  momentscatter];
endfor;

# savecolumns(allMomentsScatter, 'stattables/uncorr-momentscatter-by-modemsae.mat', "min fq median mean tq max");
savecolumns(allMomentsScatter, 'stattables/corr-momentscatter-by-modemsae.mat', "min fq median mean tq max");
